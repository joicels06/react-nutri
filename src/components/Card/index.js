import react from 'react';
import './style.css'

export default function Card(props){

    const item = props.item;


    return(
        <div key={item.id} className="post">
    
          <strong className="titulo"> {item.titulo}</strong>
    
          <img src={item.capa} alt={item.titulo} className="capa"/>
          <p className="subtitulo">
            {item.subtitulo}
          </p>
          {props.onAdd  &&  <button className={props.checkInSide ? "botao botao--disable" : "botao"} onClick={()=>props.onAdd()}>Favorito</button>}
          {props.onDel  &&  <button className="botao" onClick={()=>props.onDel()}>Remover</button>}
        </div>
      )
}



