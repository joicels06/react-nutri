import React, {useEffect, useState} from 'react'
import Card from './components/Card'
import './style.css'

export default function App(){
  const [nutri, setNutri] = useState([]);
  const [favoritos, setFavoritos] = useState([]);

  useEffect(() => {

    function loadApi(){
      let url = 'https://sujeitoprogramador.com/rn-api/?api=posts';
      fetch(url)
      .then((r)=>r.json())
      .then((json)=>{
        console.log(json)
        setNutri(json)
      })
    }
    loadApi()

  }, [])


  function onAdd(item){
    let fav = [...favoritos, item];    
    setFavoritos(fav)
  }

  function onDel(id){
    let fav = favoritos.filter((item)=> item.id != id);
    setFavoritos(fav)
  }

  function checkInSide(id){
    let found = favoritos.find((item)=> item.id === id);
    return(
      found
    )
  }


  return(
   
      <div className="container">
        <header>
        <h1>NUTRI</h1>
        </header>

        <div className="content"   >
        <div className="containersub">
        <h2 className="titulos">Lista</h2>
        {nutri.map((item)=> {
          return(
            <Card key={item.id} item={item} checkInSide={checkInSide(item.id)} onAdd={()=> onAdd(item)}/>
          )
         
          
        })}
        </div>

        <div className="containersub">
          <h2 className="titulos">Favoritos</h2>
        {favoritos.map((item)=> {
          return(
            <Card key={item.id} item={item}  onDel={()=> onDel(item.id)}/>
          )
        })}
        </div>
        </div>

        
      </div>

  ) 
}